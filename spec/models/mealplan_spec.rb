# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Mealplan, type: :model do
  describe 'relationships' do
    it { should belong_to(:user) }
    it { should have_many(:recipes) }
    it do
      should validate_inclusion_of(:status)
        .in_array(Mealplan::VALID_STATUSES)
    end

    it { should validate_presence_of(:name) }
  end
end
