# frozen_string_literal: true

FactoryBot.define do
  factory :mealplan do
    user
    name { Faker::TvShows::StarTrek.character }
    status { 'pending' }

    trait :with_notes do
      notes { Faker::TvShows::RickAndMorty.quote }
    end
  end
end
