# frozen_string_literal: true

FactoryBot.define do
  factory :mealplan_recipe do
    mealplan_id { create(:mealplan).id }
    recipe_id { create(:recipe).id }
    user_id { create(:user).id }
  end
end
