# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    sequence(:email) { |n| "#{n}#{Faker::Internet.email}#{n}" }
    password { Faker::Internet.password(min_length: 8) }
  end
end
