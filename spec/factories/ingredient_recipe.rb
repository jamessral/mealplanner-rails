# frozen_string_literal: true

FactoryBot.define do
  factory :ingredient_recipe do
    ingredient_id { create(:ingredient).id }
    recipe_id { create(:recipe).id }
    quantity { Faker::Number.number(digits: 1) }
    user_id { create(:user).id }
  end
end
