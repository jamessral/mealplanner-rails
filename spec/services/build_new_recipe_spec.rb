# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ::Services::Recipes::BuildNewRecipe do
  describe 'with valid inputs' do
    it 'adds values from params and sets' do
      user = create(:user)
      recipe_params = {
        name: 'name',
        description: 'description',
        ingredient_recipes_attributes: [
          ingredient_id: 24,
          quantity: 42
        ]
      }
      recipe = ::Services::Recipes::BuildNewRecipe
               .new(params: recipe_params, user: user).call

      expect(recipe.name).to eq(recipe_params[:name])

      expect(recipe.description).to eq(recipe_params[:description])

      expect(recipe.ingredient_recipes[0].quantity)
        .to eq(recipe_params[:ingredient_recipes_attributes][0][:quantity])

      expect(recipe.ingredient_recipes[0].attributes['ingredient_id'])
        .to eq(recipe_params[:ingredient_recipes_attributes][0][:ingredient_id])
    end
  end
end
