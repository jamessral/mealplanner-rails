# frozen_string_literal: true

require 'rails_helper'

describe Services::Mealplans::Create do
  context 'with valid user' do
    it 'returns success and saved mealplan' do
      user = create(:user)
      recipe_ids = create_list(:recipe, 2).collect(&:id)
      params = { name: Faker::Food.dish }

      result = Services::Mealplans::Create.new(user: user,
                                               recipe_ids: recipe_ids,
                                               params: params).call
      mealplan = result.fetch(:mealplan)
      expect(result.success?).to be(true)
      expect(mealplan.created_at.present?).to be(true)
      expect(mealplan.valid?).to be(true)
    end

    it 'attaches recipes to newly created mealplan' do
      user = create(:user)
      recipe_ids = create_list(:recipe, 3, user: user).map(&:id)
      mealplan_params = { name: Faker::TvShows::StarTrek.character }
      result = Services::Mealplans::Create.new(params: mealplan_params,
                                               user: user,
                                               recipe_ids: recipe_ids).call
      mealplan = result.fetch(:mealplan)
      expect(result.success?).to be(true)
      expect(mealplan.created_at.present?).to be(true)
      mealplan.recipes.each do |recipe|
        expect(recipe_ids.include?(recipe.id)).to be(true)
      end
    end
  end
end
