# frozen_string_literal: true

require 'rails_helper'

RSpec.feature 'Ingredients', type: :feature do
  feature 'Create Ingredient' do
    include ActionView::Helpers::NumberHelper

    scenario 'User creates ingredient' do
      user = create(:user)
      login_as(user)

      visit new_ingredient_path
      name = Faker::TvShows::SiliconValley.character
      description = Faker::TvShows::SiliconValley.quote
      price = Faker::Number::number
      unit = Faker::Lorem.word

      fill_in 'Name', with: name
      fill_in 'Description', with: description
      fill_in 'Price', with: price
      fill_in 'Unit', with: unit

      click_button 'Create'
      expect(page).to have_text(name)
      expect(page).to have_text(description)


      expect(page).to have_text(number_to_currency(price, precision: 2))
      expect(page).to have_text(unit)
      expect(page).to have_button('edit')
      expect(page).to have_button('delete')
    end
  end
end
