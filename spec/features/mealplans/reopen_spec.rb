# frozen_string_literal: true

require 'rails_helper'

RSpec.feature 'Reopen Mealplan', type: :feature do
  context "when user visits mealplans/completed" do
    it "allows user to reopen completed mealplan" do
      user = create(:user)
      sign_in user
      mealplan = create(:mealplan, user: user, status: 'completed')
      visit mealplans_path(mealplan_id: mealplan.id)
      click_on('Show Completed')
      click_on('Reopen')
      expect(page).not_to have_content('Reopen')
    end
  end
end
