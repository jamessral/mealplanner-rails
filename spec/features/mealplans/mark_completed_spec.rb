# frozen_string_literal: true

require 'rails_helper'

RSpec.feature 'Mealplans::MarkCompleted', type: :feature do
  context 'when user visits mealplans list' do
    it 'allows marking as completd' do
      user = create(:user)
      sign_in user
      mealplan = create(:mealplan, user: user)
      visit mealplans_path
      click_on('Complete')
      created_date = ::MealplansHelper.friendly_date(mealplan.created_at)
      expect(page).not_to have_text("Mealplan for week of #{created_date}")
    end
  end
end
