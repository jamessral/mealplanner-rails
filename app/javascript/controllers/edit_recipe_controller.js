import { Controller } from 'stimulus'

export default class extends Controller {
  static targets = [
    'addIngredientButton',
    'availableIngredients',
    'ingredientGroup',
    'ingredientTemplate',
    'ingredient',
  ]

  connect() {
    const availableIngredientsData = JSON.parse(
      this.data.get('availableIngredients')
    )
    this.data.set('availableIngredientsData', availableIngredientsData)

    if (availableIngredientsData.length <= 0) {
      this.addIngredientButtonTarget.hidden = true
    }
  }

  addIngredient(event) {
    event.preventDefault()
    const availableIngredientsData = this.data.get('availableIngredientsData')
    if (this.ingredientTargets.length < availableIngredientsData.length) {
      const newGroup = document.importNode(
        this.ingredientTemplateTarget.content,
        true
      )
      this.ingredientGroupTarget.appendChild(newGroup)
      this.correctNameIds()
    }

    if (availableIngredientsData.length <= 0) {
      this.addIngredientButtonTarget.hidden = false
    }
  }

  removeIngredient(event) {
    event.preventDefault()
    const toRemove = event.target.parentElement
    this.ingredientGroupTarget.removeChild(toRemove)
    this.correctNameIds()

    const previousAvailableIngredientsData = this.data.get(
      'availableIngredientsData'
    )
    const availableIngredientsData = this.data.set(
      'availableIngredients',
      previousAvailableIngredientsData[0]
    )
    if (availableIngredientsData.length <= 0) {
      this.addIngredientButtonTarget.disabled = true
    }
  }

  correctNameIds() {
    const childArray = Array.from(this.ingredientGroupTarget.children)
    for (let index = 0; index < childArray.length; index++) {
      const node = this.ingredientGroupTarget.children[index]
      this.getIngredientSelect(node).name = this.getSelectName(index)
      this.getIngredientInput(node).name = this.getInputName(index)
    }
  }

  getSelectName(index) {
    return `recipe[ingredient_recipes_attributes][${index}][ingredient_id]`
  }

  getInputName(index) {
    return `recipe[ingredient_recipes_attributes][${index}][quantity]`
  }

  getIngredientSelect(ingredientDiv) {
    return ingredientDiv.children[0]
  }

  getIngredientInput(ingredientDiv) {
    return ingredientDiv.children[1]
  }
}
