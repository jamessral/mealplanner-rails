import { Controller } from 'stimulus'

export default class extends Controller {
  static targets = ['recipe', 'recipeList', 'recipeSelect']

  connect() {
    this.recipeIndex = 0
  }

  addNewRecipe(event) {
    event.preventDefault()
    const recipeName = this.recipeSelectTarget.innerText
    const recipeId = this.recipeSelectTarget.value
    const newRecipeRow = this.buildRecipeRow(recipeName, recipeId)
    this.recipeListTarget.appendChild(newRecipeRow)
    this.recipeIndex += 1
  }

  buildRecipeRow(recipeName = '', recipeId) {
    const newRecipeRow = document.createElement('li')
    newRecipeRow.setAttribute('data-target', `${this.identifier}.recipe`)
    newRecipeRow.setAttribute('data-action', `${this.identifier}#removeItem`)
    newRecipeRow.innerHTML = `
      <input type="hidden" name="mealplan[mealplan_recipes_attributes][${
        this.recipeIndex
      }][id]" value="${recipeId}" />
      <span>${recipeName}</span>
    `
    return newRecipeRow
  }
}
