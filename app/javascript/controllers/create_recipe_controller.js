import { Controller } from 'stimulus'

export default class extends Controller {
  static targets = [
    'availableIngredients',
    'ingredientGroup',
    'ingredientTemplate',
  ]

  addIngredient(event) {
    event.preventDefault()
    const newGroup = document.importNode(
      this.ingredientTemplateTarget.content,
      true
    )
    this.ingredientGroupTarget.appendChild(newGroup)
    this.correctNameIds()
  }

  removeIngredient(event) {
    event.preventDefault()
    const toRemove = event.target.parentElement
    this.ingredientGroupTarget.removeChild(toRemove)
    this.correctNameIds()
  }

  correctNameIds() {
    const childArray = Array.from(this.ingredientGroupTarget.children)
    for (let index = 0; index < childArray.length; index++) {
      const node = this.ingredientGroupTarget.children[index]
      this.getIngredientSelect(node).name = this.getSelectName(index)
      this.getIngredientInput(node).name = this.getInputName(index)
    }
  }

  getSelectName(index) {
    return `recipe[ingredient_recipes_attributes][${index}][ingredient_id]`
  }

  getInputName(index) {
    return `recipe[ingredient_recipes_attributes][${index}][quantity]`
  }

  getIngredientSelect(ingredientDiv) {
    return ingredientDiv.children[0]
  }

  getIngredientInput(ingredientDiv) {
    return ingredientDiv.children[1]
  }
}
