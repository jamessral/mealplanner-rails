import Rails from 'rails-ujs'
import Turbolinks from 'turbolinks'
import { Application } from 'stimulus'
import { definitionsFromContext } from 'stimulus/webpack-helpers'
import '../css/application.scss'

Rails.start()
Turbolinks.start()

// Stimulus

const application = Application.start()
// eslint-disable-next-line
const context = require.context('controllers', true, /.js$/)
application.load(definitionsFromContext(context))

// End Stimulus
