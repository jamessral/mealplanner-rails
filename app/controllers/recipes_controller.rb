# frozen_string_literal: true

class RecipesController < ApplicationController
  before_action :require_user
  before_action :set_recipe, only: [:show, :edit, :update, :destroy]

  def index
    @recipes = current_user.recipes.includes(:ingredients)
  end

  def show; end

  def edit
    set_edit_params
  end

  def new
    @recipe = Recipe.new
    @recipe.ingredient_recipes << IngredientRecipe.new
    @available_ingredients = current_user.ingredients
  end

  def create
    @recipe = Recipe.new(recipe_params)
    @recipe.user = current_user
    result = ::Services::Recipes::Create.new(
      recipe: @recipe,
      params: recipe_params
    ).call
    if result
      redirect_to recipes_path
    else
      flash_errors(@recipe.errors)
      render :new
    end
  end

  def update
    @recipe.user = current_user
    success = ::Services::Recipes::Update.new(
      recipe: @recipe,
      params: recipe_params
    ).call

    if success && @recipe.save
      redirect_to recipe_path(@recipe)
    else
      set_edit_params
      flash_errors(@recipe.errors)
      render :edit
    end
  end

  def destroy
    if @recipe.destroy
      redirect_to recipes_path
    else
      render :show
    end
  end

  private

  def set_recipe
    @recipe = current_user.recipes
                          .includes(:ingredients, :ingredient_recipes)
                          .find(params[:id])
  end

  def set_edit_params
    @available_ingredients = ::Services::Recipes::MapAvailableIngredients
                             .new(recipe: @recipe, user_id: current_user.id)
                             .call
    @ingredients = ::Services::Recipes::MapIngredientsEditData
                   .new(recipe: @recipe).call
  end

  def recipe_params
    params.require(:recipe)
          .permit(:name,
                  :description,
                  ingredient_recipes_attributes: [
                    :ingredient_id,
                    :quantity
                  ])
  end
end
