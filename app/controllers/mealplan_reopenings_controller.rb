class MealplanReopeningsController < ApplicationController
  def create
    if mealplan.reopen
      redirect_to mealplans_url
    else
      redirect_to mealplans_url, alert: 'Unable to reopen mealplan'
    end
  end

  private

  def mealplan
    @mealplan ||= Mealplan.find(params[:mealplan_id])
  end
end
