class HomeController < ApplicationController
  def index
    redirect_to mealplans_path
  end
end
