# frozen_string_literal: true

###
# Top Level Mealplan class that contains recipes
# over time
##
class MealplansController < ApplicationController
  before_action :require_user

  DEFAULT_ERROR_MESSAGE = 'Something went wrong'

  def index
    @mealplans = mealplans
    @showing_completed = params[:completed]
  end

  def show
    @mealplan = mealplan
  end

  def new
    @mealplan = Mealplan.new
    @available_recipes = current_user.recipes
  end

  def edit
    @mealplan = mealplan
  end

  def create
    result = Services::Mealplans::Create.new(params: mealplan_params,
                                             user: current_user,
                                             recipe_ids: create_recipe_ids).call
    if result.failed?
      redirect_to new_mealplan_path, alert: result.error_message
    else
      redirect_to mealplans_path,
                  notice: 'Mealplan was successfully created.'
    end
  end

  def update
    if mealplan.update(mealplan_params)
      redirect_to mealplan, notice: 'Mealplan was successfully updated.'
    else
      flash_errors
      redirect_to edit_mealplan_url(mealplan)
    end
  end

  def destroy
    if mealplan.destroy
      redirect_to mealplans_url, notice: 'Mealplan was successfully destroyed.'
    else
      flash[:alert] = mealplan&.errors&.full_message || DEFAULT_ERROR_MESSAGE
      redirect_to mealplan_url(mealplan)
    end
  end

  private

  def mealplans
    @mealplans = if params[:completed]
                   current_user.mealplans.completed.includes(:recipes)
                 else
                   current_user.mealplans.pending.includes(:recipes)
                 end
  end

  def mealplan
    @mealplan ||= current_user.mealplans
                              .includes(:recipes, :mealplan_recipes)
                              .find(params[:id])
  end

  def mealplan_params
    params.require(:mealplan)
          .permit(:completed,
                  :name,
                  :notes,
                  :user,
                  mealplan_recipes_attributes: :id)
  end

  def create_recipe_ids
    mapped_ids = params.dig('mealplan', 'mealplan_recipes_attributes')
          &.values
          &.map { |v| v['id'] }

    mapped_ids || []
  end
end
