# frozen_string_literal: true

##
# Basic api controller for Ingredients CRUD actions
##
class IngredientsController < ApplicationController
  before_action :require_user

  # GET /ingredients
  # GET /ingredients.json
  def index
    require_user
    @ingredients = current_user.ingredients.includes(:recipes)
  end

  # GET /ingredients/1
  # GET /ingredients/1.json
  def show
    @ingredient = current_user.ingredients.find(params[:id])
  end

  def edit
    @ingredient = current_user.ingredients.find(params[:id])
  end

  def new
    @ingredient = Ingredient.new
  end

  # POST /ingredients
  # POST /ingredients.json
  def create
    @ingredient = Ingredient.new(ingredient_params)
    @ingredient.user = current_user

    if @ingredient.save
      redirect_to ingredient_path(@ingredient)
    else
      flash_errors(@ingredient.errors)
      render 'new'
    end
  end

  # PATCH/PUT /ingredients/1
  # PATCH/PUT /ingredients/1.json
  def update
    @ingredient = current_user.ingredients.find(params[:id])
    if @ingredient.update(ingredient_params)
      redirect_to ingredient_path(@ingredient)
    else
      flash_errors(@ingredient.errors)
      render 'update'
    end
  end

  # DELETE /ingredients/1
  # DELETE /ingredients/1.json
  def destroy
    @ingredient = current_user.ingredients.find(params[:id])
    if @ingredient.destroy
      redirect_to ingredients_path
    else
      render 'show'
    end
  end

  private

  def ingredient_params
    params.require(:ingredient)
          .permit(:name, :description, :price, :recipes, :unit)
  end
end
