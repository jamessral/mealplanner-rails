# frozen_string_literal: true

class MealplanCompletionsController < ApplicationController
  def index
    mealplans
  end

  def create
    if mealplan.complete
      redirect_to mealplans_path
    else
      redirect_to mealplans_path, alert: 'Unable to complete'
    end
  end

  private

  def mealplans
    @mealplans || Mealplan.completed
  end

  def mealplan
    @mealplan || Mealplan.find(params[:mealplan_id])
  end
end
