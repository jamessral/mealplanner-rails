# frozen_string_literal: true

module MealplansHelper
  def self.friendly_date(date)
    date.strftime('%B %d, %Y')
  end
end
