# frozen_string_literal: true

class IngredientRecipe < ApplicationRecord
  belongs_to :ingredient
  belongs_to :recipe
  belongs_to :user

  validates :quantity, presence: true

  def ingredient_name
    ingredient&.name
  end

  def ingredient_id
    ingredient&.id
  end

  def ingredient_unit
    ingredient&.unit
  end
end
