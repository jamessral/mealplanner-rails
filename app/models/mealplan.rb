# frozen_string_literal: true

##
# Top-level class for mealplan, a collection of recipes
##
class Mealplan < ApplicationRecord
  belongs_to :user
  has_many :mealplan_recipes
  has_many :recipes, through: :mealplan_recipes
  accepts_nested_attributes_for :mealplan_recipes

  VALID_STATUSES = %w[
    pending
    completed
  ].freeze

  validates :status, inclusion: { in: VALID_STATUSES }
  validates :name, presence: true

  def complete
    self.status = 'completed'
    save
  end

  def reopen
    self.status = 'pending'
    save
  end

  def pending?
    status == 'pending'
  end

  def completed?
    status == 'completed'
  end

  scope :pending, -> { where(status: 'pending') }
  scope :completed, -> { where(status: 'completed') }
end
