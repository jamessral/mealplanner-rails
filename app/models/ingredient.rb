# frozen_string_literal: true

##
# Base Ingredient model
##
class Ingredient < ApplicationRecord
  belongs_to :user
  has_many :ingredient_recipes
  has_many :recipes, through: :ingredient_recipes, inverse_of: :ingredients

  validates :name, presence: true
  validates :price, presence: true
end
