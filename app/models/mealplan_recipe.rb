# frozen_string_literal: true

class MealplanRecipe < ApplicationRecord
  belongs_to :mealplan
  belongs_to :recipe
  belongs_to :user
end
