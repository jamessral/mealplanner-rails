# frozen_string_literal: true

module Services
  module Recipes
    ###
    #   Object to add appropriate ingredients
    ##
    class Update
      attr_reader :sequence
      def initialize(recipe:, params:)
        @sequence = ActionSequence::Sequence.new(
          actions: [initialize_association,
                    ensure_user,
                    params_with_user,
                    update_recipe],
          initial_context: {
            recipe: recipe,
            params: params,
            ingredient_recipes_params: params[:ingredient_recipes_attributes]
                                           .to_h
                                           .with_indifferent_access
          }
        )
      end

      def call
        @sequence.call
      end

      private

      def initialize_association
        ->(c) { c.fetch(:recipe).ingredient_recipes = [] }
      end

      def ensure_user
        lambda do |c|
          c.fail_context!('recipe missing user') if c.fetch(:recipe)&.user.nil?
        end
      end

      def params_with_user
        lambda do |c|
          recipe = c.fetch(:recipe)
          new_params = c.fetch(:ingredient_recipes_params)&.map do |params_array|
            updated_params = params_array[1]
                             .to_h
                             .with_indifferent_access
                             .merge(user_id: recipe.user.id, recipe_id: recipe.id)
            [params_array[0], updated_params]
          end

          c.fetch(:params)
           .merge!(ingredient_recipes_attributes: new_params.to_h)
        end
      end

      def update_recipe
        lambda do |c|
          params = c.fetch(:params, [])
          c.fetch(:recipe).update(params.to_h)
        end
      end
    end
  end
end
