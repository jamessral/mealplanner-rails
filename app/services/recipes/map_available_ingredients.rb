# frozen_string_literal: true

module ::Services
  module Recipes
    class MapAvailableIngredients
      def initialize(recipe:, user_id:)
        @recipe = recipe
        @user_id = user_id
      end

      def call
        ingredients = User.find(@user_id).ingredients.includes(:ingredient_recipes)
        ingredients - @recipe.ingredients
      end
    end
  end
end
