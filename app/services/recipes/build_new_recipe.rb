# frozen_string_literal: true

module Services
  module Recipes
    ###
    # Service responsible for setting up recipe and
    # optionally adding a provided user
    ##
    class BuildNewRecipe
      attr_reader :params, :user

      def initialize(params:, user:)
        @params = params
        @user = user
      end

      def call
        return false if user.nil?

        recipe = Recipe.new(params)
        recipe.user = user
        recipe
      end
    end
  end
end
