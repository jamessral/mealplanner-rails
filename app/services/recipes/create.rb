# frozen_string_literal: true

module Services
  module Recipes
    ###
    #   Object to add appropriate ingredients
    ##
    class Create
      attr_reader :recipe, :params, :ingredient_recipes_params

      def initialize(recipe:, params:)
        @recipe = recipe
        @params = params
        @ingredient_recipes_params = params[:ingredient_recipes_attributes]
                                     .to_h
                                     .with_indifferent_access
      end

      def call
        set_ingredient_users
        recipe.save
      end

      private

      def set_ingredient_users
        recipe.ingredient_recipes.each do |ing|
          ing.user_id = recipe.user.id
        end
      end
    end
  end
end
