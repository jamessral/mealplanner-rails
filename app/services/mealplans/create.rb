# frozen_string_literal: true

module Services
  module Mealplans
    class Create
      attr_reader :service_organizer

      def initialize(params:, user:, recipe_ids:)
        initial_context = {
          params: params.to_h.with_indifferent_access,
          user: user,
          recipe_ids: recipe_ids
        }
        @service_organizer =
          ::ActionSequence::Sequence.new(actions: actions,
                                         initial_context: initial_context)
      end

      def call
        service_organizer.call
      end

      def actions
        [
          initialize_mealplan,
          ensure_recipe_ids_present,
          set_mealplan_recipes,
          ensure_mealplan_saves
        ]
      end

      def initialize_mealplan
        lambda { |c|
          mealplan = Mealplan.new(name: c.fetch(:params)['name'],
                                  notes: c.fetch(:params)['notes'],
                                  status: 'pending',
                                  user: c.fetch(:user))

          c.fail_context!("Can't initialize mealplan") unless mealplan.save
          c.add_to_context!(mealplan: mealplan)
        }
      end

      def ensure_recipe_ids_present
        lambda { |c|
          recipe_ids = c.fetch(:recipe_ids)
          if recipe_ids.nil? || recipe_ids.empty?
            c.fail_context!('Must have at least one recipe')
          end
        }
      end

      def set_mealplan_recipes
        lambda { |c|
          c.fetch(:recipe_ids).each do |id|
            mealplan = c.fetch(:mealplan)
            mealplan.mealplan_recipes <<
              MealplanRecipe.new(recipe_id: id,
                                 mealplan_id: mealplan.id,
                                 user_id: c.fetch(:user).id)
          end
        }
      end

      def ensure_mealplan_saves
        lambda { |c|
          unless c.fetch(:mealplan).save
            c.fail_context!('Unable to save mealplan')
          end
        }
      end
    end
  end
end
