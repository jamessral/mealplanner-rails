class AddNotesAndNameToMealplans < ActiveRecord::Migration[5.2]
  def change
    add_column :mealplans, :name, :string, null: false
    add_column :mealplans, :notes, :text, null: true

    add_index :mealplans, :name
  end
end
