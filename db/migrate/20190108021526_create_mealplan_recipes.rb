# frozen_string_literal: true

class CreateMealplanRecipes < ActiveRecord::Migration[5.2]
  def change
    create_table :mealplan_recipes do |t|
      t.belongs_to :mealplan, index: true
      t.belongs_to :recipe, index: true
    end
  end
end
