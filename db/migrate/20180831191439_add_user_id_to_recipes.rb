class AddUserIdToRecipes < ActiveRecord::Migration[5.2]
  def change
    change_table :recipes do |t|
      t.belongs_to :user
    end
  end
end
