# frozen_string_literal: true

class AddUserIdToMealplanRecipes < ActiveRecord::Migration[5.2]
  def change
    add_reference :mealplan_recipes, :user, foreign_key: true
  end
end
