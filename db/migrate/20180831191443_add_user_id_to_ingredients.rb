class AddUserIdToIngredients < ActiveRecord::Migration[5.2]
  def change
    change_table :ingredients do |t|
      t.belongs_to :user
    end
  end
end
