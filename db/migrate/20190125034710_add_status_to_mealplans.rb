# frozen_string_literal: true

class AddStatusToMealplans < ActiveRecord::Migration[5.2]
  def change
    add_column :mealplans, :status, :string
  end
end
