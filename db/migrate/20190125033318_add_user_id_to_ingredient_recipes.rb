# frozen_string_literal: true

class AddUserIdToIngredientRecipes < ActiveRecord::Migration[5.2]
  def change
    add_reference :ingredient_recipes, :user, foreign_key: true
  end
end
