# frozen_string_literal: true

require 'sidekiq/web'

Rails.application.routes.draw do
  devise_for :users
  root to: 'mealplans#index'
  post 'user_token' => 'user_token#create'

  if Rails.env.development?
    mount Sidekiq::Web, at: 'sidekiq'
  end

  post '/graphql', to: 'graphql#execute'
  resources :recipes
  resources :ingredients
  resources :mealplans do
    resources :mealplan_completions, only: %i[create]
    resources :mealplan_reopenings, only: %i[create]
  end

  namespace 'api' do
    namespace 'v1' do
      resources :users
      resources :recipes
      resources :ingredients
    end
  end
end
